/**
 * Created by u1238538 on 1-9-14.
 */

module.exports = function(grunt) {
    // Project configuration.

    var license = '/* ===================================================================*\ \n \
    Copyright (c) <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> BetweenBrands \n \
    \n \
    Permission is hereby granted, free of charge, to any person\n  \
    obtaining a copy of this software and associated documentation\n \
    files (the "Software"), to deal in the Software without\n \
    restriction, including without limitation the rights to use,\n \
    copy, modify, merge, publish, distribute, sublicense, and/or sell\n \
    copies of the Software, and to permit persons to whom the\n \
    Software is furnished to do so, subject to the following\n \
    conditions:\n \
    \n \
    The above copyright notice and this permission notice shall be\n \
    included in all copies or substantial portions of the Software.\n \
    \n \
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,\n \
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES\n \
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n \
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT\n \
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,\n \
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\n \
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR\n \
    OTHER DEALINGS IN THE SOFTWARE.\n \
\* ===================================================================*/\n';



    /*
    * TODO : img optimization, css minifyer
    * */

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy : {
            main : {
                files :[
                    {expand: true, cwd: 'app/resources', src: ['images/**'], dest: 'assets'}
                ]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: [
                    { expand : true, cwd : 'resources/scss',src:['*.scss'],dest : 'assets/css',ext : '.css'}
                ]
            }
        },
        uglify: {
            options: {
                banner: license
            },
            js: {
                src: 'resources/js/*.js',
                dest: 'assets/js/script.min.js'
            }
        },
        clean : {
            main :['assets/js','assets/css','assets/images'],
            js : ['assets/js/script.min.js'],
            scss : ['assets/css/'],
            copy : ['assets/images']
        },
        watch: {
            scss: {
                files: 'resources/scss/*.scss',
                tasks: ['clean:scss','sass'],
                options: {
                    interrupt: true
                }
            },
            js : {
                files: 'resources/js/*.js',
                tasks: ['clean:js','uglify:js'],
                options: {
                    interrupt: true
                }
            },
            copy : {
                files: ['resources/images/*'],
                tasks: ['clean:copy','copy'],
                options: {
                    interrupt: true
                }
            },
        }
    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-watch');// Before u can run these tasks u need to run npm install grunt-contrib-watch --save
    grunt.loadNpmTasks('grunt-contrib-uglify');//npm install grunt-contrib-uglify --save
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');


    // Default task(s).
    grunt.registerTask('default', ['clean','uglify','copy','sass']);
};