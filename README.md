# README #

     Copyright (c) ToerClan! 2014-10-17 DualBrands ft. BetweenIT

This README describes which steps you need to take in order to get the application running on your machine.

## How do I get set up? ##
The following guide assumes you have NodeJS, and GruntJS running on your machine. Please see our further readings section for a thorough documentation on these projects.

### Step 1 ###
Create and open your working directory in you favorite CLI.

     mkdir /path/to/your/project/
     cd /path/to/your/project/

### Step 2 ###
Pull the repository.

     git init
     git add remote origin https://[username]@bitbucket.org/betweenbrands/toerclan.git
     git pull origin master

### Step 3 ###
Install the Node modules, using Node's package manager.

     npm install .

### Step 4 ###
Install the bower components.

     bower install .

### Step 5 ###
Run the grunt file, to build the assets folder

     grunt

Tip: Run grunt watch, when you are working. This command will watch the resources folder and will build an assets folder when the resources are updated!

### Step 6 ###
There is no step 6

Have fun!


## Further reading ##
* [NodeJS](http://nodejs.org/)
* [Bower](http://bower.io/)
* [GruntJS](http://gruntjs.com/)
* [Polymer](https://www.polymer-project.org/)

handcrafted by [DualBrands](http://Dualbrands.nl/) featuring [BetweenIT](http://betweenit.nl/)